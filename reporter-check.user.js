// ==UserScript==
// @name         Other reported issues
// @version      0.1
// @description  Provide a link to the related issue
// @author       Richie Gee
// @include      https://projects.mbww.com/browse/IPGMBSUP-*
// @include      https://projects.mbww.com/projects/IPGMBSUP/queues/issue/IPGMBSUP-*
// @run-at      document-end
// ==/UserScript==

(function() {
    'use strict';
        var element = document.querySelectorAll("[id*='reporter']")
        var i
        var reporter
        for (i = 0; i < element.length; i++){
            reporter = element[1].attributes.rel.value
        }
        var people_div = document.getElementById('peopledetails')
        var reported_issues = '<dl><dt>Reported Issues:</dt><dd><p><a href="https://projects.mbww.com/issues/?jql=project%20%3D%20IPGMBSUP%20and%20reporter%20%3D%20&quot;'+ reporter +'&quot; ORDER BY created DESC" target="_blank">Click to Search</a></p></dd></dl>'
        people_div.innerHTML += reported_issues;
})();
